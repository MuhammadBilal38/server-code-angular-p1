"use strict";

var _Express = _interopRequireDefault(require("Express"));

var _bodyParser = _interopRequireDefault(require("body-parser"));

var _cors = _interopRequireDefault(require("cors"));

var _connection = _interopRequireDefault(require("./Database/connection.js"));

var _addData = _interopRequireDefault(require("./Database/addData.js"));

var _getData = _interopRequireDefault(require("./Database/getData.js"));

var _deleteData = _interopRequireDefault(require("./Database/deleteData.js"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var port = 3000;
var app = (0, _Express["default"])();
app.use(_bodyParser["default"].json());
;
;
app.use((0, _cors["default"])()); // Get

app.get("/table", function (req, res) {
  (0, _getData["default"])().then(function (data) {
    console.log(data);
    res.send(data);
  });
}); //post

app.post("/register", function (req, res) {
  (0, _addData["default"])(req.body);
  res.status(200).send({
    message: "okokoKkO"
  });
}); //delete

app["delete"]('/delete', function (req, res) {
  (0, _deleteData["default"])('5f987863-b91d-46f2-871e-23b7f7bdc8fc');
  res.status(200).send({
    message: 'ssAadas'
  });
}); //listen Result

app.listen(port, function () {
  console.log("Listening at port:" + port);
});
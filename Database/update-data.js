import db from './connection.js'
const {
    personalInfo,
    metricInfo,
    collegeInfo,
    uniInfo
} = db.models;

export default (id, userdata) => {
    console.log('**************/n/n/n/n/n/n/n/n//n/n/n/n/n/n/', id);
    console.log(userdata)
    return personalInfo.update({
        firstname: userdata.firstname,
        lastname: userdata.lastname,
        email: userdata.email,
        phone: userdata.phone,
        address: userdata.address,
        city: userdata.city,
        gender: userdata.gender,
    }, {
        where: {
            person_id: id
        }
    }).then(
        metricInfo.update({
            insitute: userdata.insituteMetric,
            majors: userdata.majorMetric,
            totalMarks: userdata.totalmarksMetric,
            obtainMarks: userdata.obtainmarksMetric,
            percentage: userdata.percentageMetric
        }, {
            where: {
                personalInfoPersonId: id
            }
        }).then(
            collegeInfo.update({
                insitute: userdata.insituteCollege,
                majors: userdata.majorCollege,
                totalMarks: userdata.totalmarksCollege,
                obtainMarks: userdata.obtainmarksCollege,
                percentage: userdata.percentageCollege
            }, {
                where: {
                    personalInfoPersonId: id
                }
            }).then(
                uniInfo.update({
                    insitute: userdata.insituteUni,
                    degree: userdata.degree,
                    CGPA: userdata.cgpa
                }, {
                    where: {
                        personalInfoPersonId: id
                    }
                })
            )
        )
    )

}
import db from './connection.js'
const {
    personalInfo,
    metricInfo,
    collegeInfo,
    uniInfo
} = db.models;

export default () => {
    return personalInfo.findAll({
        include: [metricInfo, collegeInfo, uniInfo],

    })
}
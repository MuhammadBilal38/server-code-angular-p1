import Sequelize from 'sequelize'

export default (Db) => {
    Db.define('metricInfo', {
        metricInfo_id: {
            type: Sequelize.UUID,
            defaultValue: Sequelize.UUIDV4,
            allowNull: false,
            primaryKey: true
        },
        insitute: {
            type: Sequelize.STRING
        },
        majors: {
            type: Sequelize.STRING
        },
        totalMarks: {
            type: Sequelize.STRING
        },
        obtainMarks: {
            type: Sequelize.STRING
        },
        percentage: {
            type: Sequelize.STRING
        }
    });
}
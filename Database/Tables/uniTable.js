import Sequelize from 'sequelize'

export default (Db) => {
    Db.define('uniInfo', {
        uniInfo_id: {
            type: Sequelize.UUID,
            defaultValue: Sequelize.UUIDV4,
            allowNull: false,
            primaryKey: true
        },
        insitute: {
            type: Sequelize.STRING
        },
        degree: {
            type: Sequelize.STRING
        },
        CGPA: {
            type: Sequelize.STRING
        }
    });
}
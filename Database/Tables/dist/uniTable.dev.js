"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _sequelize = _interopRequireDefault(require("sequelize"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = function _default(Db) {
  Db.define('uniInfo', {
    uniInfo_id: {
      type: _sequelize["default"].UUID,
      defaultValue: _sequelize["default"].UUIDV4,
      allowNull: false,
      primaryKey: true
    },
    insitute: {
      type: _sequelize["default"].STRING
    },
    degree: {
      type: _sequelize["default"].STRING
    },
    CGPA: {
      type: _sequelize["default"].STRING
    }
  });
};

exports["default"] = _default;
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _sequelize = _interopRequireDefault(require("sequelize"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = function _default(DB) {
  DB.define('personalInfo', {
    person_id: {
      type: _sequelize["default"].UUID,
      defaultValue: _sequelize["default"].UUIDV4,
      allowNull: false,
      primaryKey: true
    },
    firstname: {
      type: _sequelize["default"].STRING
    },
    lastname: {
      type: _sequelize["default"].STRING
    },
    email: {
      type: _sequelize["default"].STRING
    },
    phone: {
      type: _sequelize["default"].STRING
    },
    address: {
      type: _sequelize["default"].STRING
    },
    city: {
      type: _sequelize["default"].STRING
    },
    gender: {
      type: _sequelize["default"].STRING
    }
  });
};

exports["default"] = _default;
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _sequelize = _interopRequireDefault(require("sequelize"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = function _default(Db) {
  var table = Db.define('collegeInfo', {
    collegeInfo_id: {
      type: _sequelize["default"].UUID,
      defaultValue: _sequelize["default"].UUIDV4,
      allowNull: false,
      primaryKey: true
    },
    insitute: {
      type: _sequelize["default"].STRING
    },
    majors: {
      type: _sequelize["default"].STRING
    },
    totalMarks: {
      type: _sequelize["default"].STRING
    },
    obtainMarks: {
      type: _sequelize["default"].STRING
    },
    percentage: {
      type: _sequelize["default"].STRING
    }
  });
};

exports["default"] = _default;
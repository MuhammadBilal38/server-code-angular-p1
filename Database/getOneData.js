import db from './connection.js'
const {
    personalInfo,
    metricInfo,
    collegeInfo,
    uniInfo
} = db.models;

export default (id) => {
    console.log(id)
    return personalInfo.findOne({
        where: {
            person_id: id
        },
        include: [metricInfo, collegeInfo, uniInfo],

    })

}
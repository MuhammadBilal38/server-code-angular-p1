import db from './connection.js'
export default (userdata) => {
    const {
        personalInfo,
        metricInfo,
        collegeInfo,
        uniInfo
    } = db.models;

    personalInfo.create({
        firstname: userdata.firstname,
        lastname: userdata.lastname,
        email: userdata.email,
        phone: userdata.phone,
        address: userdata.address,
        city: userdata.city,
        gender: userdata.gender,
        metricInfo: {
            insitute: userdata.insituteMetric,
            majors: userdata.majorMetric,
            totalMarks: userdata.totalmarksMetric,
            obtainMarks: userdata.obtainmarksMetric,
            percentage: userdata.percentageMetric
        },
        collegeInfo: {
            insitute: userdata.insituteCollege,
            majors: userdata.majorCollege,
            totalMarks: userdata.totalmarksCollege,
            obtainMarks: userdata.obtainmarksCollege,
            percentage: userdata.percentageCollege
        },
        uniInfo: {
            insitute: userdata.insituteUni,
            degree: userdata.degree,
            CGPA: userdata.cgpa

        }

    }, {
        include: [metricInfo, collegeInfo, uniInfo]
    }).then(() => {
        console.log("successsss")
    }).catch((err) => {
        console.log(err)
    })

    console.log(db.models)

    console.log(userdata)
}
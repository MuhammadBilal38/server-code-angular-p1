"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _connection = _interopRequireDefault(require("./connection.js"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _db$models = _connection["default"].models,
    personalInfo = _db$models.personalInfo,
    metricInfo = _db$models.metricInfo,
    collegeInfo = _db$models.collegeInfo,
    uniInfo = _db$models.uniInfo;

var _default = function _default(id) {
  personalInfo.destroy({
    where: {
      person_id: id
    }
  });
};

exports["default"] = _default;
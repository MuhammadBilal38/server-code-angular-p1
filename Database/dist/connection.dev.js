"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _sequelize = _interopRequireDefault(require("sequelize"));

var _personalTable = _interopRequireDefault(require("./Tables/personalTable.js"));

var _metricTable = _interopRequireDefault(require("./Tables/metricTable.js"));

var _collegeTable = _interopRequireDefault(require("./Tables/collegeTable.js"));

var _uniTable = _interopRequireDefault(require("./Tables/uniTable.js"));

var _associtations = _interopRequireDefault(require("./associtations.js"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

//Database Connection
var db = new _sequelize["default"]("PersonInfo", "postgres", "1234", {
  host: "localhost",
  port: 5433,
  dialect: "postgres"
});
db.cors;
db.authenticate().then(function (data) {})["catch"](function (err) {
  console.error("rrrrrrrrrrrrr");
}); //Creating Table

(0, _personalTable["default"])(db);
(0, _metricTable["default"])(db);
(0, _collegeTable["default"])(db);
(0, _uniTable["default"])(db); ///Creating Association

(0, _associtations["default"])(db);
db.sync({
  force: true
});
var _default = db;
exports["default"] = _default;
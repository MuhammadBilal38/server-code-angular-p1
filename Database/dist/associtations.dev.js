"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = addAssociation;

function addAssociation(Db) {
  var _Db$models = Db.models,
      personalInfo = _Db$models.personalInfo,
      metricInfo = _Db$models.metricInfo,
      collegeInfo = _Db$models.collegeInfo,
      uniInfo = _Db$models.uniInfo;
  personalInfo.hasOne(metricInfo, {
    onDelete: 'cascade'
  });
  personalInfo.hasOne(collegeInfo, {
    onDelete: 'cascade'
  });
  personalInfo.hasOne(uniInfo, {
    onDelete: 'cascade'
  });
}
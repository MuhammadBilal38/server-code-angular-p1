"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _connection = _interopRequireDefault(require("./connection.js"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = function _default(userdata) {
  var _db$models = _connection["default"].models,
      personalInfo = _db$models.personalInfo,
      metricInfo = _db$models.metricInfo,
      collegeInfo = _db$models.collegeInfo,
      uniInfo = _db$models.uniInfo;
  personalInfo.create({
    firstname: userdata.firstname,
    lastname: userdata.lastname,
    email: userdata.email,
    phone: userdata.phone,
    address: userdata.address,
    city: userdata.city,
    gender: userdata.gender,
    metricInfo: {
      insitute: userdata.insituteMetric,
      majors: userdata.majorMetric,
      totalMarks: userdata.totalmarksMetric,
      obtainMarks: userdata.obtainmarksMetric,
      percentage: userdata.percentageMetric
    },
    collegeInfo: {
      insitute: userdata.insituteCollege,
      majors: userdata.majorCollege,
      totalMarks: userdata.totalmarksCollege,
      obtainMarks: userdata.obtainmarksCollege,
      percentage: userdata.percentageCollege
    },
    uniInfo: {
      insitute: userdata.insituteUni,
      degree: userdata.degree,
      CGPA: userdata.cgpa
    }
  }, {
    include: [metricInfo, collegeInfo, uniInfo]
  }).then(function () {
    console.log("successsss");
  })["catch"](function (err) {
    console.log(err);
  });
  console.log(_connection["default"].models);
  console.log(userdata);
};

exports["default"] = _default;
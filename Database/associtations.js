export default function addAssociation(Db) {
    const {
        personalInfo,
        metricInfo,
        collegeInfo,
        uniInfo
    } = Db.models;


    personalInfo.hasOne(metricInfo, {
        onDelete: 'cascade'

    })

    personalInfo.hasOne(collegeInfo, {
        onDelete: 'cascade'

    })

    personalInfo.hasOne(uniInfo, {
        onDelete: 'cascade'

    })
}